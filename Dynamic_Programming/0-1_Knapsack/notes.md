# 0/1 Knapsack
The premise is that there is a sack that can carry a capacity X. There is a
selection of items of given value and weight. The goal is to fill the sack and
take the items that will provide the most wealth.

## The Idea
For every item, you have a choice. You can take the item, or not. This is best
represented by a tree structure. The root node will contain our entire list,
an empty list representing the fact that nothing has been selected yet,
the total profit, and the total weight of the selected items. The tree is built
out until the list of items is empty, meaning a decision has been made for
every item in consideration. Calculating every node of this tree is the fully
recursive, brute-force solution.

## Brute Force
The brute force method is described above, and the code is available to view as
well. The downside to the brute force method is that it may solve the same sub-
problem multiple times. This can become computationally expensive, and so
methods have been devised to avoid it.

## Memoization
As the name implies, memoization means that we will take notes as we recurse,
and we will be able to directly grab results of previously solved sub-problems.

It is important to reason about what to memoize. We want to take note of what
changes. Looking to the brute-force solution sheds light on the matter. In the
recursive calls, the only values that changed were the capacity and the current
index.

The rows will be for each item we can take. The columns will be for different
capacity levels. After the base cases, we check to see if we are in a sub-
problem that's been solved already and return the result immediately if true.

Otherwise, we can continue to make our calculations. We continue to make our
decisions about whether we will take the item or not, and save the result into
the array before returning it.

## Bottom-up
The next phase of evolving the algorithm is to get rid of the recursion
altogether. The idea here is that memoizing fills a table, so why not just fill
the table iteratively instead of recursively. After all, iteratation is always
faster than recursion.

If our table is called `dp`, then `dp[i][c]` will represent the maximum
knapsack profit for capacity 'c' calculated from the first 'i' items. 

The basics of the algorithm are still the same. We will choose to either take
an item or not. 

If we choose **not** to take the item, we take the profit from excluding the
item, found at `dp[i - 1][c]`. The value from the row above does not contain
the ith item, and is the maximum profit.

If we choose to take the item, we must first check if the item's weight allows
it to be selected for each capacity `c`. The formula for this is:
`profit[i] + dp[i - 1][c - weight[i]]`. This boils down to counting the item's
profit and adding the maximum profit from any remaining capacity, which has
already been calculated.

This will fill the table, and the value in the bottom right cell is the maximum
profit to be had for a given capacity and after considering all the items.

### But Wait, There's More!
So far, we have only been able to determine the maximum profit. However, with
a full table, we can also state which items should be taken to achieve the
maximum profit.

We know that the maximum profit is found in the bottom-right corner of the
table. What we can then do is look at the cell directly above. If the value is
different, that means that we took the item in the current row. Once we've
determined that we have taken an item, we add the item to our list, and then
subtract the item's profit and go left until we find the remaining profit.

We repeat the pattern until we end up at a cell with a profit of 0.