#include <algorithm>
#include <iostream>
#include <vector>

struct Item {
  int profit = 0;
  int weight = 0;
};

class Knapsack {
 public:
  static int solve(std::vector<Item> const& items, int capacity) {
    return recursive_solve(items, capacity, 0);
  }

 private:
  static int recursive_solve(std::vector<Item> const& items, int capacity,
                             std::size_t currentIdx) {
    // Base case checks
    if (capacity <= 0 || currentIdx > items.size() - 1) {
      return 0;
    }

    // We can choose to take an item, or not. However, we can only take an item
    // if we have the capacity to do so. If we are able to take an item, we add
    // its profit to our recursive profit total.
    int takeProfit = 0;
    if (items[currentIdx].weight <= capacity) {
      takeProfit = items[currentIdx].profit +
                   recursive_solve(items, capacity - items[currentIdx].weight,
                                   currentIdx + 1);
    }

    // This is us choosing not to take an item.
    int leaveProfit = recursive_solve(items, capacity, currentIdx + 1);

    return std::max(takeProfit, leaveProfit);
  }
};

int main() {
  std::vector<Item> items{{1, 1}, {6, 2}, {10, 3}, {16, 5}};
  int maxProfit = Knapsack::solve(items, 7);
  std::cout << "Total knapsack profit ---> " << maxProfit << '\n';
  maxProfit = Knapsack::solve(items, 6);
  std::cout << "Total knapsack profit ---> " << maxProfit << '\n';
}