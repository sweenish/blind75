#include <algorithm>
#include <iostream>
#include <vector>

struct Item {
  int profit = 0;
  int weight = 0;
};

class Knapsack {
 public:
  static int solve(const std::vector<Item> &items, int capacity) {
    std::vector<std::vector<int>> dp(items.size(),
                                     std::vector<int>(capacity + 1, -1));
    return knapsackRecursive(dp, items, capacity, 0);
  }

 private:
  static int knapsackRecursive(std::vector<std::vector<int>> &dp,
                               const std::vector<Item> &items, int capacity,
                               std::size_t currentIndex) {
    // base checks
    if (capacity <= 0 || currentIndex >= items.size()) {
      return 0;
    }

    // if we have already solved a similar problem, return the result from
    // memory
    if (dp[currentIndex][capacity] != -1) {
      return dp[currentIndex][capacity];
    }

    // recursive call after choosing the element at the currentIndex
    // if the weight of the element at currentIndex exceeds the capacity, we
    // shouldn't process this
    int takeProfit = 0;
    if (items[currentIndex].weight <= capacity) {
      takeProfit =
          items[currentIndex].profit +
          knapsackRecursive(dp, items, capacity - items[currentIndex].weight,
                            currentIndex + 1);
    }

    // recursive call after excluding the element at the currentIndex
    int leaveProfit = knapsackRecursive(dp, items, capacity, currentIndex + 1);

    dp[currentIndex][capacity] = std::max(takeProfit, leaveProfit);
    return dp[currentIndex][capacity];
  }
};

int main() {
  std::vector<Item> items{{1, 1}, {6, 2}, {10, 3}, {16, 5}};
  int maxProfit = Knapsack::solve(items, 7);
  std::cout << "Total knapsack profit ---> " << maxProfit << '\n';
  maxProfit = Knapsack::solve(items, 6);
  std::cout << "Total knapsack profit ---> " << maxProfit << '\n';
}