#include <algorithm>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

struct Item {
  int profit = 0;
  int weight = 0;
  std::string name;
};

namespace Knapsack {
auto solve(const std::vector<Item>& items, int capacity)
    -> std::tuple<int, std::vector<std::string_view>> {
  // Base case checks
  if (capacity <= 0 || items.empty()) {
    return std::make_tuple<int, std::vector<std::string_view>>(0, {});
  }

  int numRows = static_cast<int>(items.size());
  std::vector<std::vector<int>> dp(numRows, std::vector<int>(capacity + 1, 0));

  // We process the first row separately because the algorithm for the rest
  // of the table needs to be able to reference the row above. This loop
  // takes the item if we reach a sufficient capacity.
  for (int c = 0; c <= capacity; c++) {
    if (items[0].weight <= c) {
      dp[0][c] = items[0].profit;
    }
  }

  // Process all sub-arrays for all the capacities
  for (int i = 1; i < numRows; i++) {
    for (int c = 1; c <= capacity; c++) {
      int profit1 = 0, profit2 = 0;
      // Include the item, if it is not more than the capacity
      if (items[i].weight <= c) {
        profit1 = items[i].profit + dp[i - 1][c - items[i].weight];
      }
      // Exclude the item
      profit2 = dp[i - 1][c];
      // Take maximum
      dp[i][c] = std::max(profit1, profit2);
    }
  }

  // Get the items that result in the maximum profit
  std::vector<std::string_view> itemNames;
  int column = capacity;
  int value = dp[dp.size() - 1][column];
  for (int row = dp.size() - 1; row > 0; --row) {
    if (value != dp[row - 1][column]) {
      itemNames.emplace_back(items[row].name);
      column -= items[row].weight;
      value -= items[row].profit;
    }
  }

  // Adds the first item if needed
  if (value != 0) {
    itemNames.emplace_back(items[0].name);
  }

  // Maximum profit will be at the bottom-right corner.
  return {dp[numRows - 1][capacity], itemNames};
}
}  // namespace Knapsack

std::ostream& operator<<(std::ostream& sout,
                         std::vector<std::string_view> const& v) {
  for (std::size_t idx = 0; idx < v.size(); ++idx) {
    sout << v[idx] << (idx + 1 == v.size() ? " " : ", ");
  }

  return sout;
}

int main() {
  std::vector<Item> items{{.profit = 1, .weight = 1, .name = "Silver Dollar"},
                          {.profit = 6, .weight = 2, .name = "Dumbbell"},
                          {.profit = 10, .weight = 3, .name = "Silver"},
                          {.profit = 16, .weight = 5, .name = "Golden Apple"}};
  auto [maxProfit, names] = Knapsack::solve(items, 7);
  std::cout << "Knapsack profit: " << maxProfit << " with " << names << '\n';
  std::tie(maxProfit, names) = Knapsack::solve(items, 6);
  std::cout << "Knapsack profit: " << maxProfit << " with " << names << '\n';
}