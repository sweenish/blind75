#include <iostream>
#include <iterator>
#include <random>
#include <vector>

int binary_search(const std::vector<int>& v, int val) {
  int firstIdx = 0;
  int lastIdx = static_cast<int>(v.size()) - 1;

  while (firstIdx <= lastIdx) {
    int midIdx = (lastIdx + firstIdx) / 2;

    if (v[midIdx] == val) {
      return midIdx;
    } else if (v[midIdx] < val) {
      firstIdx = midIdx + 1;
    } else {
      lastIdx = midIdx - 1;
    }
  }

  return -1;
}

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  for (auto it = v.begin(); it != v.end(); ++it) {
    sout << *it << (it + 1 == v.end() ? "\n" : ", ");
  }

  return sout;
}

int main() {
  std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

  for (int i = -1; i < 12; ++i) {
    int result = binary_search(v, i);
    std::cout << i << (result >= 0 ? " was found" : " was NOT found") << ".\n";
  }
}