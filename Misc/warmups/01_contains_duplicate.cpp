
/*
 * Given an array of integers, return true if any value appears at least twice
 * and false if every element is distinct.
 */

#include <iostream>
#include <unordered_set>
#include <vector>

bool contains_duplicates(std::vector<int> const& v) {
  std::unordered_set<int> uniqueElems;
  for (auto i : v) {
    auto [itr, insertSucceeded] = uniqueElems.insert(i);

    if (!insertSucceeded) {
      return true;
    }
  }

  return false;
}

int main() {
  std::cout << std::boolalpha;
  std::vector<int> nums1 = {1, 2, 3, 4};
  std::cout << contains_duplicates(nums1) << '\n';  // Expect false

  std::vector<int> nums2 = {1, 2, 3, 1};
  std::cout << contains_duplicates(nums2) << '\n';  // Expect true

  std::vector<int> nums3 = {};
  std::cout << contains_duplicates(nums3) << '\n';  // Expect false

  std::vector<int> nums4 = {1, 1, 1, 1};
  std::cout << contains_duplicates(nums4) << '\n';  // Expect true
}