/*
 * Given two strings, determine if one is a valid anagram of the other.
 */

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>

bool are_anagrams(std::string const &lhs, std::string const &rhs) {
  std::unordered_map<char, int> lhsHistogram;

  if (lhs.length() != rhs.length()) return false;

  for (auto c : lhs) {
    ++lhsHistogram[c];
  }

  for (auto c : rhs) {
    --lhsHistogram[c];
  }

  return std::all_of(lhsHistogram.begin(), lhsHistogram.end(),
                     [](auto const &itr) { return itr.second == 0; });
}

int main() {
  // Test case 1
  std::string s1 = "listen";
  std::string t1 = "silent";
  std::cout << std::boolalpha << are_anagrams(s1, t1)
            << '\n';  // Expected output: true

  // Test case 2
  std::string s2 = "hello";
  std::string t2 = "world";
  std::cout << std::boolalpha << are_anagrams(s2, t2)
            << '\n';  // Expected output: false

  // Test case 3
  std::string s3 = "anagram";
  std::string t3 = "nagaram";
  std::cout << std::boolalpha << are_anagrams(s3, t3)
            << '\n';  // Expected output: true

  // Test case 4
  std::string s4 = "rat";
  std::string t4 = "car";
  std::cout << std::boolalpha << are_anagrams(s4, t4)
            << '\n';  // Expected output: false

  // Test case 5
  std::string s5 = "";
  std::string t5 = "";
  std::cout << std::boolalpha << are_anagrams(s5, t5)
            << '\n';  // Expected output: true
}
