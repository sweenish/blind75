/*
 * A pangram is a sentence that contains every letter of the English
 * alphabet. For a given string, determine if it is a pangram or not.
 */

#include <cctype>
#include <iostream>
#include <string>
#include <unordered_set>

bool is_pangram(std::string const& s) {
  std::unordered_set<char> uniqueLetters;

  for (auto c : s) {
    if (std::isalpha(c)) {
      uniqueLetters.insert(std::tolower(c));
    }
  }

  return uniqueLetters.size() == 26;
}

int main() {
  std::cout << std::boolalpha;
  // Test case 1: "TheQuickBrownFoxJumpsOverTheLazyDog"
  // Expected output: true
  std::cout << is_pangram("TheQuickBrownFoxJumpsOverTheLazyDog") << '\n';

  // Test case 2: "This is not a pangram"
  // Expected output: false
  std::cout << is_pangram("This is not a pangram") << '\n';

  // Test case 3: "abcdef ghijkl mnopqr stuvwxyz"
  // Expected output: true
  std::cout << is_pangram("abcdef ghijkl mnopqr stuvwxyz") << '\n';

  // Test case 4: ""
  // Expected output: false
  std::cout << is_pangram("") << '\n';

  // Test case 5: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  // Expected output: true
  std::cout << is_pangram(
                   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
            << '\n';
}