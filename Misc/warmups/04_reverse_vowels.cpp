/*
 * Given a string, reverse only the vowels in the string.
 */

#include <cctype>
#include <iostream>
#include <string>

std::string reverse_vowels(std::string str) {
  auto is_vowel = [](char c) {
    constexpr std::string_view vowels{"aeiouAEIOU"};

    return vowels.find(c) != std::string::npos;
  };

  int leftIdx = 0;
  int rightIdx = str.size() - 1;

  while (leftIdx < rightIdx) {
    while (leftIdx < rightIdx && !is_vowel(str[leftIdx])) {
      ++leftIdx;
    }
    while (leftIdx < rightIdx && !is_vowel(str[rightIdx])) {
      --rightIdx;
    }

    std::swap(str[leftIdx], str[rightIdx]);
    ++leftIdx;
    --rightIdx;
  }

  return str;
}

int main() {
  std::string s1 = "hello";
  std::string expected_output1 = "holle";
  std::string actual_output1 = reverse_vowels(s1);
  std::cout << "Test Case 1: " << (expected_output1 == actual_output1) << '\n';

  std::string s2 = "DesignGUrus";
  std::string expected_output2 = "DusUgnGires";
  std::string actual_output2 = reverse_vowels(s2);
  std::cout << "Test Case 2: " << (expected_output2 == actual_output2) << '\n';

  std::string s3 = "AEIOU";
  std::string expected_output3 = "UOIEA";
  std::string actual_output3 = reverse_vowels(s3);
  std::cout << "Test Case 3: " << (expected_output3 == actual_output3) << '\n';

  std::string s4 = "aA";
  std::string expected_output4 = "Aa";
  std::string actual_output4 = reverse_vowels(s4);
  std::cout << "Test Case 4: " << (expected_output4 == actual_output4) << '\n';

  std::string s5 = "";
  std::string expected_output5 = "";
  std::string actual_output5 = reverse_vowels(s5);
  std::cout << "Test Case 5: " << (expected_output5 == actual_output5) << '\n';
}