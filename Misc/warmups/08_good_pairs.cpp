/*
 * Given an array of integers, return the number of good pairs in it.
 *
 * A pair, (i, j), is called *good* if `nums[i] == nums[j]` and `i < j`.
 */

#include <iostream>
#include <unordered_map>
#include <vector>

int count_good_pairs(std::vector<int> const& v) {
  std::unordered_map<int, int> freq;
  int numGoodPairs = 0;

  //
  // What makes this work is the counting as we go. Counting a new number means
  // it can be a pair with all the previous entries that have been counted.
  // For example, adding the first number creates 0 pairs. Adding the second
  // number creates 1 new pair. Adding the third number creates 2 *more* pairs.
  //
  for (auto const& val : v) {
    ++freq[val];
    numGoodPairs += freq[val] - 1;
  }

  return numGoodPairs;
}

int main() {
  std::vector<int> nums1 = {1, 2, 3, 1, 1, 3};
  int result1 = count_good_pairs(nums1);
  std::cout << "Result 1: " << result1 << " (Expected: 4)\n";

  std::vector<int> nums2 = {1, 1, 1, 1};
  int result2 = count_good_pairs(nums2);
  std::cout << "Result 2: " << result2 << " (Expected: 6)\n";

  std::vector<int> nums3 = {1, 2, 3};
  int result3 = count_good_pairs(nums3);
  std::cout << "Result 3: " << result3 << " (Expected: 0)\n";
}