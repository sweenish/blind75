/*
 * Given a non-negative number, x, determine square root of x rounded down to
 * nearest integer. Do not use any built-in functions.
 */

#include <cstdint>
#include <iostream>

int int_square_root(int x) {
  if (x < 2) {
    return x;
  }

  int left = 2;
  int right = x / 2;

  while (left <= right) {
    int pivot = left + (right - left) / 2;
    std::int64_t testSquare = static_cast<std::int64_t>(pivot) * pivot;

    if (testSquare < x) {
      ++left;
    } else if (testSquare > x) {
      --right;
    } else {
      return pivot;
    }
  }

  return right;
}

int main() {
  int input1 = 4;
  int expectedOutput1 = 2;
  int result1 = int_square_root(input1);
  std::cout << (result1 == expectedOutput1) << '\n';  // Expected output: 1

  int input2 = 8;
  int expectedOutput2 = 2;
  int result2 = int_square_root(input2);
  std::cout << (result2 == expectedOutput2) << '\n';  // Expected output: 1

  int input4 = 2;
  int expectedOutput4 = 1;
  int result4 = int_square_root(input4);
  std::cout << (result4 == expectedOutput4) << '\n';  // Expected output: 1

  int input5 = 3;
  int expectedOutput5 = 1;
  int result5 = int_square_root(input5);
  std::cout << (result5 == expectedOutput5) << '\n';  // Expected output: 1

  int input6 = 15;
  int expectedOutput6 = 3;
  int result6 = int_square_root(input6);
  std::cout << (result6 == expectedOutput6) << '\n';  // Expected output: 1
}