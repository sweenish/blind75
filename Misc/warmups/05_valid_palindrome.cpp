/*
 * Given a string, determine if it is a valid palindrome.
 */

#include <cctype>
#include <iostream>
#include <string>

bool is_valid_palindrome(std::string const &str) {
  std::cout << "Testing: \"" << str << "\"\n";
  int leftIdx = 0;
  int rightIdx = str.length() - 1;

  while (!std::isalpha(str[leftIdx]) &&
         leftIdx < static_cast<int>(str.length())) {
    ++leftIdx;
  }

  while (!std::isalpha(str[rightIdx]) && rightIdx > 0) {
    --rightIdx;
  }

  while (rightIdx > leftIdx) {
    if (std::tolower(str[leftIdx]) == std::tolower(str[rightIdx])) {
      while (++leftIdx && !std::isalpha(str[leftIdx])) /**/
        ;
      while (--rightIdx && !std::isalpha(str[rightIdx])) /**/
        ;
      continue;
    } else {
      return false;
    }
  }

  return true;
}

int main() {
  // Test case 1: "A man, a plan, a canal, Panama!"
  // Expected output: true
  std::cout << std::boolalpha
            << is_valid_palindrome("A man, a plan, a canal, Panama!") << '\n';

  // Test case 2: "race a car"
  // Expected output: false
  std::cout << std::boolalpha << is_valid_palindrome("race a car") << '\n';

  // Test case 3: "Was it a car or a cat I saw?"
  // Expected output: true
  std::cout << std::boolalpha
            << is_valid_palindrome("Was it a car or a cat I saw?") << '\n';

  // Test case 4: "Madam, in Eden, I'm Adam."
  // Expected output: true
  std::cout << std::boolalpha
            << is_valid_palindrome("Madam, in Eden, I'm Adam.") << '\n';

  // Test case 5: "empty string"
  // Expected output: true
  std::cout << std::boolalpha << is_valid_palindrome("") << '\n';
}
