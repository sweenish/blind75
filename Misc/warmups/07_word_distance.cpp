/*
 * Given an array of words and two words, find the shortest distance between the
 * words in the array.
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

int shortest_word_distance(std::vector<std::string> const& v,
                           std::string const& word1, std::string const& word2) {
  int idxOne = -1;
  int idxTwo = -1;
  int shortestDistance = static_cast<int>(v.size());

  int size = static_cast<int>(v.size());
  for (int i = 0; i < size; ++i) {
    bool therewasAnUpdate = false;

    if (v[i] == word1) {
      idxOne = i;
      therewasAnUpdate = true;
    }

    if (v[i] == word2) {
      idxTwo = i;
      therewasAnUpdate = true;
    }

    if (therewasAnUpdate && idxOne != -1 && idxTwo != -1) {
      shortestDistance = std::min(shortestDistance, std::abs(idxOne - idxTwo));
    }
  }

  return shortestDistance;
}

int main() {
  // Test case 1
  std::vector<std::string> words1 = {"the",  "quick", "brown", "fox", "jumps",
                                     "over", "the",   "lazy",  "dog"};
  std::string word11 = "fox";
  std::string word21 = "dog";
  int expectedOutput1 = 5;
  int actualOutput1 = shortest_word_distance(words1, word11, word21);
  std::cout << "Test case 1: " << std::boolalpha
            << (expectedOutput1 == actualOutput1) << '\n';

  // Test case 2
  std::vector<std::string> words2 = {"a", "b", "c", "d", "a", "b"};
  std::string word12 = "a";
  std::string word22 = "b";
  int expectedOutput2 = 1;
  int actualOutput2 = shortest_word_distance(words2, word12, word22);
  std::cout << "Test case 2: " << std::boolalpha
            << (expectedOutput2 == actualOutput2) << '\n';

  // Test case 3
  std::vector<std::string> words3 = {"a", "c", "d", "b", "a"};
  std::string word13 = "a";
  std::string word23 = "b";
  int expectedOutput3 = 1;
  int actualOutput3 = shortest_word_distance(words3, word13, word23);
  std::cout << "Test case 3: " << std::boolalpha
            << (expectedOutput3 == actualOutput3) << '\n';

  // Test case 4
  std::vector<std::string> words4 = {"a", "b", "c", "d", "e"};
  std::string word14 = "a";
  std::string word24 = "e";
  int expectedOutput4 = 4;
  int actualOutput4 = shortest_word_distance(words4, word14, word24);
  std::cout << "Test case 4: " << std::boolalpha
            << (expectedOutput4 == actualOutput4) << '\n';
}
