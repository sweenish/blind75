#include <iostream>

#include "basic_list.hpp"

int main() {
  SList<int> one;

  for (int i = 2; i < 11; i += 2) {
    one.push_back(i);
  }

  std::cout << one;
  reverse(one);
  std::cout << one;
}