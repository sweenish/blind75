#include <array>
#include <iostream>
#include <iterator>  // std::prev()
#include <utility>   // std::pair

template <std::size_t N>
auto find_length_without_dupes(const std::array<int, N>& c, int sum) {
  int first = 0;
  int last = static_cast<int>(c.size()) - 1;

  while (first < last) {
    int pairSum = c[first] + c[last];
    if (pairSum == sum) {
      return std::make_pair(first, last);
    }

    if (pairSum > sum) {
      --last;
    } else {
      ++first;
    }
  }

  return std::make_pair(-1, -1);
}

template <std::size_t N>
std::ostream& operator<<(std::ostream& sout, const std::array<int, N>& arr) {
  sout << "[ ";
  for (const auto& val : arr) {
    sout << val << ' ';
  }
  return sout << "]";
}

std::ostream& operator<<(std::ostream& sout, const std::pair<int, int>& p) {
  return sout << "[" << p.first << ", " << p.second << "]";
}

int main() {
  std::array<int, 5> one{1, 2, 3, 4, 6};
  std::array<int, 4> two{2, 5, 9, 11};

  std::cout << "Input: " << one
            << ", Sum: 6\nOutput: " << find_length_without_dupes(one, 6)
            << '\n';
  std::cout << "Input: " << two
            << ", Sum: 11\nOutput: " << find_length_without_dupes(two, 11)
            << '\n';
}