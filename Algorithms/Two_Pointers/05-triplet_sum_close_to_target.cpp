#include <algorithm>
#include <cmath>
#include <iostream>
#include <limits>
#include <vector>

int triplet_sum_close_to_target(std::vector<int> v, int target) {
  std::sort(v.begin(), v.end());

  int smallestDifference = std::numeric_limits<int>::max();

  for (std::size_t i = 0; i < v.size() - 2; ++i) {
    std::size_t left = i + 1;
    std::size_t right = v.size() - 1;

    while (left < right) {
      int diff = target - v[i] - v[left] - v[right];
      if (diff == 0) {
        return diff;
      }

      if (std::abs(diff) < std::abs(smallestDifference) ||
          (std::abs(diff) == std::abs(smallestDifference) &&
           diff > smallestDifference)) {
        smallestDifference = diff;
      }

      if (diff > 0) {
        ++left;
      } else {
        --right;
      }
    }
  }

  return target - smallestDifference;
}

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  sout << "[ ";
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(sout, " "));

  return sout << "]";
}

int main() {
  std::vector<int> one{-2, 0, 1, 2};
  std::vector<int> two{-3, -1, 1, 2};
  std::vector<int> three{1, 0, 1, 1};

  std::cout << "Given: " << one
            << " and target 2\nOutput: " << triplet_sum_close_to_target(one, 2)
            << "\n\n";
  std::cout << "Given: " << two
            << " and target 2\nOutput: " << triplet_sum_close_to_target(two, 1)
            << "\n\n";
  std::cout << "Given: " << three << " and target 2\nOutput: "
            << triplet_sum_close_to_target(three, 100) << "\n\n";
}