# Two Pointers
Two pointers can be used to find a pair or subarray in a **sorted** container
that meets certain conditions. This can reduce a brute force `O(n^2)` to `O(n)`.

## Types of Problems
- Pair with target sum
- Remove duplicates
- Squaring a sorted array
- Triplet sum to zero

## General Ideas
In cases where you start at opposite ends, you don't want the first to overtake
the last iterator.

Another use case is when one pointer walks the container, and the second is used
to track a position based on a condition.