#include <iostream>
#include <iterator>  // std::prev()
#include <vector>

std::vector<int> calc_sorted_squares(const std::vector<int>& v) {
  int left = 0;
  int right = static_cast<int>(v.size()) - 1;
  int highestSquareIdx = right;

  std::vector<int> sortedSquares(v.size());

  while (left <= right) {
    int leftSquare = v[left] * v[left];
    int rightSquare = v[right] * v[right];

    if (leftSquare > rightSquare) {
      sortedSquares[highestSquareIdx] = leftSquare;
      ++left;
    } else {
      sortedSquares[highestSquareIdx] = rightSquare;
      --right;
    }
    --highestSquareIdx;
  }

  return sortedSquares;
}

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  sout << "[ ";
  for (auto val : v) {
    sout << val << ' ';
  }
  return sout << "]";
}

int main() {
  std::vector<int> one{-2, -1, 0, 2, 3};
  std::vector<int> two{-3, -1, 0, 1, 2};

  std::cout << "Given: " << one << "\nOutput: " << calc_sorted_squares(one)
            << "\n\n";
  std::cout << "Given: " << two << "\nOutput: " << calc_sorted_squares(two)
            << '\n';
}