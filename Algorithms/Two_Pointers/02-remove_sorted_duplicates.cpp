#include <array>
#include <iostream>

template <std::size_t N>
int find_length_without_dupes(std::array<int, N>& c) {
  int nextNonDupeLoc = 1;
  for (std::size_t walker = 0; walker < c.size(); ++walker) {
    if (c[nextNonDupeLoc - 1] != c[walker]) {
      c[nextNonDupeLoc] = c[walker];
      ++nextNonDupeLoc;
    }
  }

  return nextNonDupeLoc;
}

template <std::size_t N>
std::ostream& operator<<(std::ostream& sout, const std::array<int, N>& arr) {
  sout << "[ ";
  for (const auto& val : arr) {
    sout << val << ' ';
  }
  return sout << "]";
}

int main() {
  std::array one{2, 3, 3, 3, 6, 9, 9};
  std::array two{2, 2, 2, 11};

  std::cout << "Input: " << one
            << "\nOutput: " << find_length_without_dupes(one) << '\n';
  std::cout << "Input: " << two
            << "\nOutput: " << find_length_without_dupes(two) << '\n';
}