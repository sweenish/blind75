#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

class TripletSumToZero {
 public:
  static std::vector<std::vector<int>> find_triplets(std::vector<int> v) {
    std::vector<std::vector<int>> sets;

    std::sort(v.begin(), v.end());

    for (std::size_t i = 0; i < v.size() - 2; ++i) {
      if (i > 0 && v[i] == v[i - 1]) {
        continue;
      }

      find_pair(v, -v[i], i + 1, sets);
    }

    return sets;
  }

 private:
  static void find_pair(const std::vector<int> v, int targetSum, int leftIdx,
                        std::vector<std::vector<int>>& sets) {
    int rightIdx = static_cast<int>(v.size()) - 1;

    while (leftIdx < rightIdx) {
      int sum = v[leftIdx] + v[rightIdx];

      if (sum == targetSum) {
        sets.emplace_back(
            std::vector<int>{-targetSum, v[leftIdx], v[rightIdx]});
        ++leftIdx;
        --rightIdx;

        while (leftIdx < rightIdx && v[leftIdx] == v[leftIdx - 1]) {
          ++leftIdx;
        }
        while (leftIdx < rightIdx && v[rightIdx] == v[rightIdx + 1]) {
          --rightIdx;
        }
      } else if (sum < targetSum) {
        ++leftIdx;
      } else {
        --rightIdx;
      }
    }
  }
};

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  sout << "[ ";
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(sout, " "));

  return sout << "]";
}

std::ostream& operator<<(std::ostream& sout,
                         const std::vector<std::vector<int>>& c) {
  for (auto it = c.begin(); it != c.end(); ++it) {
    sout << *it << ((it + 1 == c.end()) ? "\n" : ", ");
  }

  return sout;
}

int main() {
  std::vector<int> one{-3, 0, 1, 2, -1, 1, -2};
  std::vector<int> two{-5, 2, -1, -2, 3};

  std::cout << "Given: " << one
            << "\nOutput: " << TripletSumToZero::find_triplets(one) << "\n\n";
  std::cout << "Given: " << two
            << "\nOutput: " << TripletSumToZero::find_triplets(two) << "\n\n";
}