# Fast & Slow Pointers
Two pointers that move through a list or array at different speeds. If the data
structure is cyclic, they are bound to meet.

## Types of Problems
- Linked List has a cycle
    - The fast pointer will eventually lap the slow pointer and they will be
    equivalent if there is a cycle, otherwise the fast pointer hits the end
    first.
- Start of Linked List cycle
    - Use the same algorithm as above to find the cycle. Once the cycle is
    found, the slow pointer must be in the cycle. Using slow as a reference,
    the size of the cycle can be determined.

    Then, from the beginning, jump the fast pointer ahead 'size-of-cycle' times.
    This places it the same distance from the cycle start as the head of the
    list is. Now incrementing both slow and fast by one, they will meet at the
    start.
- Happy Number
    - The rules of a happy number are two sum the squares of the digits, and
    continue doing so  until you arrive at 1 or a previously found number.

    The result is always a cycle. Instead of pointers, the fast number evaluates
    two steps at at time while slow does one. Because 1^2 is 1, all
    possibilities end in a cycle. The number is happy if fast and slow meet at
    1.
- Middle of Linked List
    - The fast pointer moves through the list twice as fast as the slow pointer.
    When the fast pointer reaches the end of list, slow is naturally in the
    middle.


## General Idea
If there is a cycle in the data structure (usually a linked list), the fast
pointer will eventually lap the slow pointer. If not, the fast pointer will
hit the end well before the slow pointer.