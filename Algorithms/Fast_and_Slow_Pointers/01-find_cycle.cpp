#include <iostream>

#include "basic_list.hpp"

/*
 * Exercise solution function has_cycle() is implemented in basic_list.hpp
 */

int main() {
  SList<int> one;
  one.push_back(1);
  one.push_back(2);
  one.push_back(3);
  one.push_back(4);
  one.push_back(5);
  one.push_back(6);

  std::cout << "Given: " << one << "Output: " << std::boolalpha
            << has_cycle(one) << "\n\n";

  one.create_cycle_from_end(3);
  std::cout << "Given: " << one << "Output: " << std::boolalpha
            << has_cycle(one);
}