#include <iostream>

class IsHappyNumber {
 public:
  static bool find(int val) {
    int slow = val;
    int fast = val;

    do {
      slow = find_sum_of_digit_squares(slow);
      fast = find_sum_of_digit_squares(find_sum_of_digit_squares(fast));
    } while (slow != fast);

    return slow == 1;
  }

 private:
  static int find_sum_of_digit_squares(int val) {
    int sum = 0;
    while (val > 0) {
      int digit = val % 10;
      sum += digit * digit;

      val /= 10;
    }

    return sum;
  }
};

int main() {
  std::cout << "Given: 23\nOutput: " << std::boolalpha
            << IsHappyNumber::find(23) << "\n\n";
  std::cout << "Given: 12\nOutput: " << std::boolalpha
            << IsHappyNumber::find(12) << "\n\n";
}