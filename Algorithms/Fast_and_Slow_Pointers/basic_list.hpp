#ifndef BASIC_LIST_HPP
#define BASIC_LIST_HPP

#include <iostream>
#include <utility>

template <typename T>
class SList {
 public:
  SList() = default;
  SList(const SList& other);
  SList(SList&& other);
  ~SList();

  void push_back(T val);
  void create_cycle_from_end(T start);

  SList& operator=(SList rhs);

  friend void swap(SList& lhs, SList& rhs) {
    using std::swap;

    std::swap(lhs.m_head, rhs.m_head);
    std::swap(lhs.m_tail, rhs.m_tail);
  }

  friend std::ostream& operator<<(std::ostream& sout, const SList<T>& slist) {
    Node* walker = slist.m_head;
    while (walker) {
      sout << walker->data << (walker == slist.m_tail ? "\n" : ", ");
      if (walker == slist.m_tail) {
        break;
      } else {
        walker = walker->next;
      }
    }

    sout << (slist.m_tail->next == nullptr ? "No cycle" : "Has a cycle")
         << '\n';

    return sout;
  }

  // Exercise Solution is here
  friend bool has_cycle(const SList& slist) {
    Node* fast = slist.m_head;
    Node* slow = slist.m_head;

    while (fast != nullptr && fast->next != nullptr) {
      fast = fast->next->next;
      slow = slow->next;

      if (slow == fast) {
        return true;
      }
    }

    return false;
  }

 private:
  struct Node {
    T data = T();
    Node* next = nullptr;

    Node() = default;
    Node(T val) : data(val) {}
  };

  Node* m_head = nullptr;
  Node* m_tail = nullptr;
};

// Implementation
template <typename T>
SList<T>::SList(const SList& other) {
  Node* walker = other.m_head;

  while (walker) {
    push_back(walker->data);
    walker = walker->next;
  }
}

template <typename T>
SList<T>::SList(SList&& other) : m_head(other.m_head), m_tail(other.m_tail) {
  other.m_head = nullptr;
  other.m_tail = nullptr;
}

template <typename T>
SList<T>::~SList() {
  while (m_head != m_tail) {
    Node* tmp = m_head;
    m_head = m_head->next;
    delete tmp;
  }

  delete m_tail;
  m_tail = nullptr;
}

template <typename T>
void SList<T>::push_back(T val) {
  if (!m_head) {
    m_head = new Node(val);
    m_tail = m_head;
    return;
  }

  m_tail->next = new Node(val);
  m_tail = m_tail->next;
}

template <typename T>
void SList<T>::create_cycle_from_end(T start) {
  Node* walker = m_head;
  while (walker && walker->data != start) {
    walker = walker->next;
  }

  if (walker) {
    m_tail->next = walker;
  }
}

template <typename T>
SList<T>& SList<T>::operator=(SList rhs) {
  swap(*this, rhs);

  return *this;
}

#endif