#include <iostream>
#include <iterator>
#include <string>
#include <unordered_map>
#include <vector>

std::vector<std::size_t> find_anagrams(const std::string& str,
                                       const std::string& pattern) {
  std::unordered_map<char, int> patternFreq;

  for (const auto& c : pattern) {
    ++patternFreq[c];
  }

  std::vector<std::size_t> foundStartingIndices;
  std::size_t matchedChars = 0;
  std::size_t windowStart = 0;
  for (std::size_t windowEnd = 0; windowEnd < str.size(); ++windowEnd) {
    char rightChar = str[windowEnd];
    if (patternFreq.find(rightChar) != patternFreq.end()) {
      --patternFreq[rightChar];

      if (patternFreq[rightChar] == 0) {
        ++matchedChars;
      }
    }

    if (matchedChars == pattern.size()) {
      foundStartingIndices.push_back(windowStart);
    }

    if (windowEnd >= pattern.size() - 1) {
      char leftChar = str[windowStart];
      ++windowStart;
      if (patternFreq.find(leftChar) != patternFreq.end()) {
        ++patternFreq[leftChar];

        if (patternFreq[leftChar] != 0) {
          --matchedChars;
        }
      }
    }
  }

  return foundStartingIndices;
}

std::ostream& operator<<(std::ostream& sout,
                         const std::vector<std::size_t>& v) {
  sout << "[ ";
  for (const auto& i : v) {
    sout << i << ' ';
  }
  return sout << "]\n";
}

int main() {
  std::string one{"ppqp"};
  std::string two{"abbcabc"};

  std::cout << "Given: " << one
            << " with pattern pq\nOutput: " << find_anagrams(one, "pq") << '\n';
  std::cout << "Given: " << two
            << " with pattern abc\nOutput: " << find_anagrams(two, "abc")
            << '\n';
}