#include <algorithm>  // std::min(), std::copy
#include <iostream>
#include <iterator>  // std::distance, std::osream_iterator
#include <limits>
#include <vector>

int findMaxSubarraySum(const int k, const std::vector<int>& v) {
  auto windowStart = v.begin();
  int windowSum = 0;
  int minWindow = std::numeric_limits<int>::max();

  for (auto windowEnd = v.begin(); windowEnd != v.end(); ++windowEnd) {
    windowSum += *windowEnd;

    while (windowSum >= k) {
      minWindow =
          std::min(minWindow,
                   static_cast<int>(std::distance(windowStart, windowEnd)) + 1);
      windowSum -= *windowStart;
      ++windowStart;
    }
  }

  return minWindow == std::numeric_limits<int>::max() ? 0 : minWindow;
}

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  sout << "{ ";
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(sout, " "));
  sout << "}";

  return sout;
}

int main() {
  std::vector<int> one{2, 1, 5, 2, 3, 2};
  std::vector<int> two{2, 1, 5, 2, 8};
  std::vector<int> three{3, 4, 1, 1, 6};

  std::cout << "Given:\n\t" << one << "\nMaximum subarray sum with K = 7: "
            << findMaxSubarraySum(7, one) << "\n\n";
  std::cout << "Given:\n\t" << two << "\nMaximum subarray sum with K = 7: "
            << findMaxSubarraySum(7, two) << '\n';
  std::cout << "Given:\n\t" << three << "\nMaximum subarray sum with K = 8: "
            << findMaxSubarraySum(8, three) << '\n';
}