#include <algorithm>  // std::max(), std::copy
#include <iostream>
#include <iterator>  // std::distance, std::osream_iterator
#include <string>
#include <unordered_map>

int findMaxSubarrayDistinctChars(const int k, const std::string& v) {
  auto windowStart = v.begin();
  int maxSubStrLength = 0;
  std::unordered_map<char, int> freq;
  std::size_t K = k;

  for (auto windowEnd = v.begin(); windowEnd != v.end(); ++windowEnd) {
    ++freq[*windowEnd];

    while (freq.size() > K) {
      --freq[*windowStart];
      if (freq[*windowStart] == 0) {
        freq.erase(*windowStart);
      }
      ++windowStart;
    }

    maxSubStrLength =
        std::max(maxSubStrLength,
                 static_cast<int>(std::distance(windowStart, windowEnd)) + 1);
  }

  return maxSubStrLength;
}

int main() {
  std::string one{"araaci"};
  std::string two{"araaci"};
  std::string three{"cbbebi"};

  std::cout << "Given:\n\t" << one << "\nLongest subarray with K = 2: "
            << findMaxSubarrayDistinctChars(2, one) << "\n\n";
  std::cout << "Given:\n\t" << two << "\nMaximum subarray sum with K = 1: "
            << findMaxSubarrayDistinctChars(1, two) << '\n';
  std::cout << "Given:\n\t" << three << "\nMaximum subarray sum with K = 3: "
            << findMaxSubarrayDistinctChars(3, three) << '\n';
}