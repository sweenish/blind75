# Sliding Window
A pattern where sub-arrays are looked at not as distinct, but more like a small
window that we slide along the array. Immediate benefits include not having to
look at overlapping elements more than once. In fact, this is a main identifier
of when the sliding window might be desirable.

## Types of problems
- Maximum subarray sum
- Minimum length subarray that achieves at least a sum
- Longest substring with K distinct characters
    - Longest subarray with K distince characters (no code written)
- Longest substring of distinct characters

## General Ideas
The start and end of the window begin at the start of the array. The start
should never overtake the end. The window grows by advancing the end of the
window. Every time the window grows, the controlling condition is checked. What
happens next is also problem dependent. But generally, the window can continue
to grow as long as the condition holds; update the result variable. If the
condition fails, we must shrink the window by advancing the start pointer until
the condition is satisfied again.

If the window is fixed size, the start and end will start moving in lock-step.
If it's not, the start may only move when a certain condition is met.

## Problem Breakdowns
### Maximum subarray sum
**Problem Statement**: Given a subarray of size `k`, find the maximum sum.

**Algorithm**: The window start and end are at the beginning of the array. Grow
the window to the correct size, summing as you go. Once the window size is hit,
the sum is compared to the current max and changed if needed. Subtract the value
at the window start and then advance the start. Continue until the window end
reaches the end of the array.

### Minimum Length Subarray That Achieves at Least a Sum
