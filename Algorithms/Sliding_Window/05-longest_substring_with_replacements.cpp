#include <algorithm>  // std::max()
#include <iostream>
#include <iterator>  // std::distance
#include <string>
#include <unordered_map>

int findLongestSubarrayWithReplacements(const std::string& v, int k) {
  auto windowStart = v.begin();
  std::unordered_map<char, int> freq;
  int maxFreq = 0;
  int result = 0;

  for (auto windowEnd = v.begin(); windowEnd != v.end(); ++windowEnd) {
    ++freq[*windowEnd];
    maxFreq = std::max(maxFreq, freq[*windowEnd]);

    if (std::distance(windowStart, windowEnd) + 1 - maxFreq > k) {
      --freq[*windowStart];
      ++windowStart;
    }

    result = std::max(
        result, static_cast<int>(std::distance(windowStart, windowEnd)) + 1);
  }

  return result;
}

int main() {
  std::string one{"aabccbb"};
  std::string two{"abbcb"};
  std::string three{"abccde"};

  std::cout << "Given:\n\t" << one << "\nLongest Substring after " << 2
            << " replacements: " << findLongestSubarrayWithReplacements(one, 2)
            << "\n\n";
  std::cout << "Given:\n\t" << two << "\nLongest Substring after " << 1
            << " replacements: " << findLongestSubarrayWithReplacements(two, 1)
            << '\n';
  std::cout << "Given:\n\t" << three << "\nLongest Substring after " << 1
            << " replacements: "
            << findLongestSubarrayWithReplacements(three, 1) << '\n';
}