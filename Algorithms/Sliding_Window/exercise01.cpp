#include <algorithm>  // std::max(), std::copy
#include <iostream>
#include <iterator>  // std::distance
#include <string>
#include <unordered_map>

bool findPermutation(const std::string& str, const std::string& pattern) {
  auto windowStart = str.begin();
  std::unordered_map<char, int> patternFreqCount;
  std::size_t matched = 0;

  for (auto c : pattern) {
    ++patternFreqCount[c];
  }

  for (auto windowEnd = str.begin(); windowEnd != str.end(); ++windowEnd) {
    if (patternFreqCount.find(*windowEnd) != patternFreqCount.end()) {
      --patternFreqCount[*windowEnd];

      if (patternFreqCount[*windowEnd] == 0) {
        ++matched;
      }
    }

    if (matched == patternFreqCount.size()) {
      return true;
    }

    if (static_cast<std::size_t>(std::distance(windowStart, windowEnd) + 1) >=
        pattern.length()) {
      if (patternFreqCount.find(*windowStart) != patternFreqCount.end()) {
        if (patternFreqCount[*windowStart] == 0) {
          --matched;
        }
        ++patternFreqCount[*windowStart];
      }
      ++windowStart;
    }
  }

  return false;
}

int main() {
  std::string one{"oidbcaf"};
  std::string two{"odicf"};
  std::string three{"bcdxabcdy"};
  std::string four{"aaacb"};

  std::cout << "Given: " << one << "\nwith pattern \"abc\": " << std::boolalpha
            << findPermutation(one, "abc") << "\n\n";
  std::cout << "Given: " << two << "\nwith pattern \"dc\": " << std::boolalpha
            << findPermutation(two, "dc") << "\n\n";
  std::cout << "Given: " << three
            << "\nwith pattern \"bcdyabcdx\": " << std::boolalpha
            << findPermutation(three, "bcdyabcdx") << "\n\n";
  std::cout << "Given: " << four << "\nwith pattern \"abc\": " << std::boolalpha
            << findPermutation(four, "abc") << "\n\n";
}