#include <algorithm>  // std::max(), std::copy
#include <iostream>
#include <iterator>  // std::distance, std::osream_iterator
#include <string>
#include <unordered_map>

int findMaxSubarrayDistinctChars(const std::string& v) {
  int windowStart = 0;
  int maxSubStrLength = 0;
  std::unordered_map<char, int> lastFoundIdx;

  for (int windowEnd = 0; windowEnd < static_cast<int>(v.size()); ++windowEnd) {
    char rightChar = v[windowEnd];

    if (lastFoundIdx.find(rightChar) != lastFoundIdx.end()) {
      windowStart = std::max(windowStart, lastFoundIdx[rightChar] + 1);
    }

    lastFoundIdx[rightChar] = windowEnd;
    maxSubStrLength = std::max(maxSubStrLength, windowEnd - windowStart + 1);
  }

  return maxSubStrLength;
}

int main() {
  std::string one{"aabccbb"};
  std::string two{"abbbb"};
  std::string three{"abccde"};

  std::cout << "Given:\n\t" << one << "\nLongest fully distinct subarray: "
            << findMaxSubarrayDistinctChars(one) << "\n\n";
  std::cout << "Given:\n\t" << two << "\nLongest fully distinct subarray: "
            << findMaxSubarrayDistinctChars(two) << '\n';
  std::cout << "Given:\n\t" << three << "\nLongest fully distinct subarray: "
            << findMaxSubarrayDistinctChars(three) << '\n';
}