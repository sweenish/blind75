#include <algorithm>  // std::max(), std::copy
#include <iostream>
#include <iterator>  // std::distance, std::osream_iterator
#include <vector>

int findMaxSubarraySum(const int k, const std::vector<int>& v) {
  auto windowStart = v.begin();
  int windowSum = 0;
  int maxSum = 0;

  for (auto windowEnd = v.begin(); windowEnd != v.end(); ++windowEnd) {
    windowSum += *windowEnd;

    if (std::distance(windowStart, windowEnd) >= k - 1) {
      maxSum = std::max(maxSum, windowSum);
      windowSum -= *windowStart;
      ++windowStart;
    }
  }

  return maxSum;
}

std::ostream& operator<<(std::ostream& sout, const std::vector<int>& v) {
  sout << "{ ";
  std::copy(v.begin(), v.end(), std::ostream_iterator<int>(sout, " "));
  sout << "}";

  return sout;
}

int main() {
  std::vector<int> one{2, 1, 5, 1, 3, 2};
  std::vector<int> two{2, 3, 4, 1, 5};

  std::cout << "Given:\n\t" << one << "\nMaximum subarray sum with K = 3: "
            << findMaxSubarraySum(3, one) << "\n\n";
  std::cout << "Given:\n\t" << two << "\nMaximum subarray sum with K = 2: "
            << findMaxSubarraySum(2, two) << '\n';
}