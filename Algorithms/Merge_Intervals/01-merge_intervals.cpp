#include <algorithm>
#include <iostream>
#include <vector>

struct Interval {
  int start = 0;
  int end = 0;

  Interval(int start, int end) : start(start), end(end) {}
};

bool operator<(const Interval& lhs, const Interval& rhs) {
  return lhs.start < rhs.start;
}

std::ostream& operator<<(std::ostream& sout, const Interval& i) {
  return sout << '{' << i.start << ", " << i.end << '}';
}

std::ostream& operator<<(std::ostream& sout, const std::vector<Interval>& v) {
  for (auto it = v.begin(); it != v.end(); ++it) {
    sout << *it << (it + 1 == v.end() ? "\n" : ", ");
  }

  return sout;
}

// Pass by value to preserve the original; the safer assumption
std::vector<Interval> merge_intervals(std::vector<Interval> v) {
  if (v.size() < 2) {
    return v;
  }

  std::sort(v.begin(), v.end());

  std::vector<Interval> merged;

  auto itr = v.begin();
  int start = itr->start;
  int end = itr->end;
  while (++itr != v.end()) {
    if (itr->start <= end) {
      end = std::max(itr->end, end);
    } else {
      merged.emplace_back(start, end);
      start = itr->start;
      end = itr->end;
    }
  }

  merged.emplace_back(start, end);

  return merged;
}

int main() {
  std::vector<Interval> one{{1, 3}, {2, 5}, {7, 9}};
  std::vector<Interval> two{{6, 7}, {2, 4}, {5, 9}};
  std::vector<Interval> three{{1, 4}, {2, 6}, {3, 5}};

  std::cout << "Given: " << one << "Output: " << merge_intervals(one) << '\n';
  std::cout << "Given: " << two << "Output: " << merge_intervals(two) << '\n';
  std::cout << "Given: " << three << "Output: " << merge_intervals(three)
            << '\n';
}