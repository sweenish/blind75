# Tech Interview Prep
A location to save my notes and code samples for tech interview preparation.

## Resources
- [Tech Interview Handbook](https://www.techinterviewhandbook.org)
- [Grokking the Coding Interview](https://designgurus.org/course/grokking-the-coding-interview)
