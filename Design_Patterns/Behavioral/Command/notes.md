# Command Pattern
## AKA
Action, Transaction

## The Idea
Turn a request into a stand-alone object that contains all information about
the request. Passing requests in this manner makes them more portable, gives
you greater control over the timing (queue or delay), and provides support for
undoable operations.

## When To Employ

