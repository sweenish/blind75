#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class Type {
 public:
  Type() = default;
  virtual ~Type() = default;

  virtual void what() const = 0;
};

class TypeFactory {
 public:
  using TCreateMethod = std::unique_ptr<Type> (*)();

  TypeFactory() = delete;

  static bool Register(std::string const& name, TCreateMethod createFunc);

  static std::unique_ptr<Type> Create(std::string const& name);

 private:
  static std::map<std::string, TCreateMethod>& GetRegistry();
};
std::map<std::string, TypeFactory::TCreateMethod>& TypeFactory::GetRegistry() {
  static std::map<std::string, TypeFactory::TCreateMethod> registry;

  return registry;
}

//
// TypeFactory implementation
//
// The static order initialization fiasco states that static initialization can
// occur in any order. The maps are not guaranteed to exist before the bools
// that allow self-registration. This is avoid by putting the maps behind a
// function. This is the "construct on first use" idiom.
//
bool TypeFactory::Register(std::string const& name, TCreateMethod createFunc) {
  auto& registry = GetRegistry();
  if (registry.find(name) == registry.end()) {
    registry[name] = createFunc;

    return true;
  }

  return false;
}

std::unique_ptr<Type> TypeFactory::Create(std::string const& name) {
  auto& registry = GetRegistry();
  if (auto itr = registry.find(name); itr != registry.end()) {
    return itr->second();
  }

  return nullptr;
}

//
// Type Subclasses
//
// One drawback is that the subclasses require more boilerplate, but my opinion
// is that this pattern is a net win. When writing a new class, you only need
// to write the new class. The factory code is untouched.
//
class Abc : public Type {
 public:
  static std::string Identify() { return "Abc"; }

  static std::unique_ptr<Type> Create() { return std::make_unique<Abc>(); }

  void what() const override { std::cout << "ABC\n"; }

 private:
  static bool isRegistered;
};
bool Abc::isRegistered = TypeFactory::Register(Abc::Identify(), Abc::Create);

class Xyz : public Type {
 public:
  static std::string Identify() { return "Xyz"; }

  static std::unique_ptr<Type> Create() { return std::make_unique<Xyz>(); }

  void what() const override { std::cout << "XYZ\n"; }

 private:
  static bool isRegistered;
};
bool Xyz::isRegistered = TypeFactory::Register(Xyz::Identify(), Xyz::Create);

class Mno : public Type {
 public:
  static std::string Identify() { return "Mno"; }

  static std::unique_ptr<Type> Create() { return std::make_unique<Mno>(); }

  void what() const override { std::cout << "MNO\n"; }

 private:
  static bool isRegistered;
};
bool Mno::isRegistered = TypeFactory::Register(Mno::Identify(), Mno::Create);

int main() {
  std::vector<std::unique_ptr<Type>> types;
  types.emplace_back(TypeFactory::Create("Xyz"));
  types.emplace_back(TypeFactory::Create("Abc"));
  types.emplace_back(TypeFactory::Create("Mno"));
  types.emplace_back(TypeFactory::Create("Jkl"));

  for (auto& i : types) {
    if (i) {
      i->what();
    } else {
      std::cout << "Unregistered Type.\n";
    }
  }
}