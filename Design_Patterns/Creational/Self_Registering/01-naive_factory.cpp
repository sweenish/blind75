#include <iostream>
#include <memory>
#include <string>
#include <vector>

class Type {
 public:
  virtual ~Type() = default;

  virtual void what() const = 0;
};

class Abc : public Type {
 public:
  void what() const override { std::cout << "ABC\n"; }
};

class Xyz : public Type {
 public:
  void what() const override { std::cout << "XYZ\n"; }
};

// The naive factory
// This type of factory works well enough for a small program, but as things
// get more complex, this becomes very unwieldy due to creating a strong
// dependency between the types and the factory.
std::unique_ptr<Type> create(std::string_view const& name) {
  if (name == "Abc") return std::make_unique<Abc>();
  if (name == "Xyz") return std::make_unique<Xyz>();

  return nullptr;
}

int main() {
  std::vector<std::unique_ptr<Type>> types;
  types.emplace_back(create("Abc"));
  types.emplace_back(create("Xyz"));

  for (auto& i : types) {
    i->what();
  }
}